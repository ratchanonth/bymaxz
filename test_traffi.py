import cv2 
import numpy as np 
import matplotlib.pyplot as plt 
from tqdm import trange
import skvideo.io

cv2.__version__

outputfile = "00.mp4" 
writer = skvideo.io.FFmpegWriter(outputfile, outputdict={
    '-vcodec': 'libx264',  #use the h.264 codec
    '-crf': '0',           #set the constant rate factor to 0, which is lossless
    '-preset':'veryslow'   #the slower the better compression, in princple, try 
                            #other options see https://trac.ffmpeg.org/wiki/Encode/H.264
    }) 
    
resize_h, resize_w = 720,1280
modelConfiguration = "cfg/yolov3.cfg";
modelWeights = "bin/yolov3.weights";
# read class names from text file
classes = None

with open("cfg/coco.names", 'rt') as f:
    classes = f.read().rstrip('\n').split('\n')
# generate different colors for different classes 
COLORS = np.random.uniform(0, 255, size=(len(classes), 3))
net = cv2.dnn.readNet(modelWeights,modelConfiguration)
#######################################################
# Initialize the parameters
confThreshold = 0.5  #Confidence threshold
nmsThreshold = 0.4   #Non-maximum suppression threshold
inpWidth = 416       #Width of network's input image
inpHeight = 416      #Height of network's input image
# initialization
class_ids = []
confidences = []
boxes = []
conf_threshold = 0.5
nms_threshold = 0.4
#color traffig
red_low=[30,150,50]
red_up=[255,255,180]
staus=None
######################################################
def rgb2color(img,low,up):
    """
    red=np.uint8([[[255,0,0]]])
    red=cv2.cvtColor(red,cv2.COLOR_RGB2HSV)
    print(red)
    """
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    lowwer=np.array(low, dtype = "uint8")
    upper=np.array(up, dtype = "uint8")
    mask =cv2.inRange(hsv,lowwer,upper)
    res=cv2.bitwise_and(img,img,mask=mask)

    cv2.imshow("img",img[:,:,:1])
    
    if np.mean(res) > 0.1: 
        return True
    else:
        return False
    
    

def prespective(img,x,y,h,w):
    pts1 = np.float32([[x,y],[h,y],[x,w],[h,w]])
    pts2 = np.float32([[0,0],[300,0],[0,300],[300,300]])
    M = cv2.getPerspectiveTransform(pts1,pts2)
    dst = cv2.warpPerspective(img,M,(300,300))
    return dst

def process(outs,confidences,class_ids,boxes):

    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                center_x = int(detection[0] * Width)
                center_y = int(detection[1] * Height)
                w = int(detection[2] * Width)
                h = int(detection[3] * Height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])

def get_output_layers(net):
    
    layer_names = net.getLayerNames()
    
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    return output_layers

# function to draw bounding box on the detected object with class name
def draw_bounding_box(img, class_id, confidence, x, y, x_plus_w, y_plus_h,red_low,red_up):
    global staus
    image=prespective(img,x,y,x_plus_w,y_plus_h)
    i=rgb2color(image,red_low,red_up)
    
    if i == True:
        staus="red"
    label = str(classes[class_id])
    
    label_all= f"{label} %0.2f {staus}"%confidence
    color = COLORS[class_id]
    
    img=cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 2)

    img=cv2.putText(img, label_all, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    staus= None
def main_draw(indices,image):
    global red_low, red_up
  
    for i in indices: 
        i = i[0]
        if classes[class_ids[i]] == "traffic light":
            box = boxes[i]
            x = int(box[0])
            y = int(box[1])
            w = int(box[2])
            h = int(box[3])
            
            draw_bounding_box(image, class_ids[i], confidences[i], round(x), round(y), round(x+w), round(y+h),red_low,red_up)
            
            
        
if __name__ == "__main__":
    """
    cap = cv2.VideoCapture("2016-03-08-01-43-06.MP4")
    length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    t = trange(length, desc='Bar desc', leave=True)
    for _ in t:
        _, frame = cap.read()
        frame= cv2.resize(frame, (resize_w, resize_h))
        Width = frame.shape[1]
        Height = frame.shape[0]
        blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416),swapRB=True, crop=False)
        net.setInput(blob)
        outs = net.forward(get_output_layers(net))
        # for each detetion from each output layer 
        # get the confidence, class id, bounding box params
        # and ignore weak detections (confidence < 0.5)
        process(outs,confidences,class_ids,boxes)
        indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
        # go through the detections remaining
        # after nms and draw bounding box
        
        main_draw(indices,frame)
        #writer.writeFrame(frame[:,:,::-1])
        # display output image    
        cv2.imshow("frame",frame)
        del confidences[:],class_ids[:],boxes[:]
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    # wait until any key is pressed
    cap.release()
    # release resources
    cv2.destroyAllWindows()
    """
    img=cv2.imread("traffic.jpg")
    Width = img.shape[1]
    Height = img.shape[0]
    blob = cv2.dnn.blobFromImage(img, 1 / 255.0, (416, 416),swapRB=True, crop=False)
    net.setInput(blob)
    outs = net.forward(get_output_layers(net))
    process(outs,confidences,class_ids,boxes)
    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
    main_draw(indices,img)
    cv2.imshow("img",img)
    cv2.waitKey(0)
