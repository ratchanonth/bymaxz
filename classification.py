from sklearn.neighbors import KNeighborsClassifier
import numpy as np
import cv2
import pickle
from sklearn.externals import joblib
class Classification():
    # load model
    def __init__(self):
        #self.lines_classification = pickle.load(open('knn_line_image_cls.pkl', 'rb'))
        #self.road_classification = pickle.load(open('knn_road_image_cls.pkl', 'rb'))
        self.lines_classification = joblib.load('clf_knn/knn_lines_joblib.sav')
        self.road_classification = joblib.load('clf_knn/knn_road_joblib.sav')
        #print('CLASS OK')

    def lines_predict(self, image,size=(32, 32)):
        flatten_image = self.resize(image)
        y_pred = self.lines_classification.predict(flatten_image)
        return y_pred

    def road_predict(self, image , size=(32, 32)):
        flatten_image = self.resize(image)
        y_pred = self.road_classification.predict(flatten_image)
        return y_pred

    def resize(self,image,size=(32, 32)):
        pixels = cv2.resize(image, size).flatten()
        # matrix image
        flatten_image = np.array(pixels)
        flatten_image = np.resize(flatten_image,(1,flatten_image.shape[0]))
        return flatten_image
