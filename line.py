import numpy as np
import cv2,math
import matplotlib.pyplot as plt
import collections
import glob
from solution_img import *
import multiprocessing 
from classification import Classification

class Line:
    """
    Class to model a lane-line.
    """
    def __init__(self, buffer_len=10):

        # flag to mark if the line was detected the last iteration
        self.detected = False

        # polynomial coefficients fitted on the last iteration
        self.last_fit_pixel = None
        self.last_fit_meter = None

        # list of polynomial coefficients of the last N iterations
        self.recent_fits_pixel = collections.deque(maxlen=buffer_len)
        self.recent_fits_meter = collections.deque(maxlen=2 * buffer_len)

        self.radius_of_curvature = None

        # store all pixels coords (x, y) of line detected
        self.all_x = None
        self.all_y = None

    def update_line(self, new_fit_pixel, new_fit_meter, detected, clear_buffer=False):
        """
        Update Line with new fitted coefficients.
        :param new_fit_pixel: new polynomial coefficients (pixel)
        :param new_fit_meter: new polynomial coefficients (meter)
        :param detected: if the Line was detected or inferred
        :param clear_buffer: if True, reset state
        :return: None
        """
        self.detected = detected

        if clear_buffer:
            self.recent_fits_pixel = []
            self.recent_fits_meter = []
        self.last_fit_pixel = new_fit_pixel
        self.last_fit_meter = new_fit_meter
        self.recent_fits_pixel.append(self.last_fit_pixel)
        self.recent_fits_meter.append(self.last_fit_meter)

    def draw(self, mask, color=(255, 0, 0), line_width=50, average=False):
        """
        Draw the line on a color mask image.
        """
        h, w, c = mask.shape

        plot_y = np.linspace(0, h - 1, h)
        
        coeffs = self.average_fit if average else self.last_fit_pixel
        
        line_center = coeffs[0] * plot_y ** 2 + coeffs[1] * plot_y + coeffs[2]
        
        line_left_side = line_center - line_width // 2
        line_right_side = line_center + line_width // 2
        # Some magic here to recast the x and y points into usable format for cv2.fillPoly()
        pts_left = np.array(list(zip(line_left_side, plot_y)))
        pts_right = np.array(np.flipud(list(zip(line_right_side, plot_y))))
        pts = np.vstack([pts_left, pts_right]) 
        src=[np.int32(pts)]
        #print(src)
        # Draw the lane onto the warped blank image
        return cv2.fillPoly(mask, src, color)

    @property
    # average of polynomial coefficients of the last N iterations
    def average_fit(self):
        return np.mean(self.recent_fits_pixel, axis=0)

    @property
    # radius of curvature of the line (averaged)
    def curvature(self):
        y_eval = 0
        coeffs = self.average_fit
        return ((1 + (2 * coeffs[0] * y_eval + coeffs[1]) ** 2) ** 1.5) / np.absolute(2 * coeffs[0])

    @property
    # radius of curvature of the line (averaged)
    def curvature_meter(self):
        y_eval = 0
        coeffs = np.mean(self.recent_fits_meter, axis=0)
        return ((1 + (2 * coeffs[0] * y_eval + coeffs[1]) ** 2) ** 1.5) / np.absolute(2 * coeffs[0])

line_rt=Line(buffer_len=10)
line_lt= Line(buffer_len=10)
img_binary=None
ym_per_pix = 30 / 720   # meters per pixel in y dimension
xm_per_pix = 3.7 / 700  # meters per pixel in x dimension
imgpoints,objpoints=[],[]
img=cv2.imread("lane/type_1.jpg")
img2=cv2.imread("lane/type_2.jpg")
time_window = 10        # results are averaged over this number of frames
mtx,dist=None,None
processed_frames=0
lane_right=0
lane_left=0
vehicle_position=0
image=np.zeros((100*2, 30*2))
#---------------------------------------------
#---------------------------------------------
def imag_pre():
    global imgpoints,objpoints
    objp = np.zeros((6*9,3), np.float32)
    objp[:,:2] = np.mgrid[0:9,0:6].T.reshape(-1,2)
    images = glob.glob('camera_cal/calibration*.jpg')
    
# Step through the list and search for chessboard corners
    for fname in images:
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        # Find the chessboard corners
        ret, corners = cv2.findChessboardCorners(gray, (9,6),None)

        # If found, add object points, image points
        if ret == True:
            objpoints.append(objp)
            imgpoints.append(corners)

            # Draw and display the corners
            img = cv2.drawChessboardCorners(img, (9,6), corners, ret)
            
    #print("OOOOK")

def test_img():
    global mtx, dist,objpoints,imgpoints
    img = cv2.imread("camera_cal/calibration2.jpg")
    
    img_size = (img.shape[1], img.shape[0])

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, img_size,None,None)
    
    return ret, mtx, dist, rvecs, tvecs

def undistort(frame, mtx, dist, verbose=False):
    """
    Undistort a frame given camera matrix and distortion coefficients.
    :param frame: input frame
    :param mtx: camera matrix
    :param dist: distortion coefficients
    :param verbose: if True, show frame before/after distortion correction
    :return: undistorted frame
    """
    frame_undistorted = cv2.undistort(frame, mtx, dist, newCameraMatrix=mtx)

    if verbose:
        fig, ax = plt.subplots(nrows=1, ncols=2)
        

    return frame_undistorted
#---------------------------------------------------------
def update_vehicle_position(frame,line_lt,line_rt):
    global xm_per_pix,vehicle_position
    y=frame.shape[0]
    kl=line_lt.last_fit_pixel
    kr=line_rt.last_fit_pixel
    xl = kl[0] * (y**2) + kl[1]* y + kl[2]
    xr = kr[0] * (y**2) + kr[1]* y + kr[2]
    pix_pos = xl + (xr - xl) / 2
    vehicle_position = (pix_pos - (frame.shape[1]/2)) * xm_per_pix
    return pix_pos
    
def chek_position_left(staus_lane):
    global vehicle_position
    if vehicle_position < -0.5:
      # 1 for ทึบ 0 for ปะ
      if staus_lane[1] == 1 :
          print("pass")
      vehicle_position_words = str(np.absolute(np.round(vehicle_position, 2))) + " m left of center"
      return vehicle_position_words
    elif vehicle_position > 0.5:
      vehicle_position_words = str(np.absolute(np.round(vehicle_position, 2))) + " m right of center"
      return vehicle_position_words
    else:
      vehicle_position_words = "at the center"
      return vehicle_position_words

def get_per_knn(img):
        h, w = img.shape[:2]
        
        
        src = np.float32([[590, 510],    # br
                [700, 510],    # bl
                [280, h-250],   # tl
                [1000, h-250]])  # tr
        dst = np.float32([[300, 300],       # br
                [0, 300],       # bl
                [0, 0],       # tl
                [300, 0]])      # tr
        M = cv2.getPerspectiveTransform(dst, src)
        dst = cv2.warpPerspective(img,M,(300,300))
       
     
        return dst,0,0

def predict_knn(predicted,img_birdeye1):
    frame= cv2.resize(img_birdeye1, (300, 300))
    
    my_class = Classification()
    if predicted == "lane":
        answer = my_class.road_predict(frame)
        return answer
    else:
        answer=my_class.road_predict(frame)
        return answer

def get_fits_by_sliding_windows(birdeye_binary, line_lt, line_rt, n_windows=9):
    """
    Get polynomial coefficients for lane-lines detected in an binary image.
    :param birdeye_binary: input bird's eye view binary image
    :param line_lt: left lane-line previously detected
    :param line_rt: left lane-line previously detected
    :param n_windows: number of sliding windows used to search for the lines
    :param verbose: if True, display intermediate output
    :return: updated lane lines and output image
    """

   
    
    height, width = birdeye_binary.shape[:2]
    #print(birdeye_binary.shape)

    # Assuming you have created a warped binary image called "binary_warped"
    # Take a histogram of the bottom half of the image
    histogram = np.sum(birdeye_binary[height//2:-30, :], axis=0)

    # Create an output image to draw on and  visualize the result
    out_img = np.dstack((birdeye_binary, birdeye_binary, birdeye_binary)) * 255
    img = np.zeros((512,512,3), np.uint8)
  

    # Find the peak of the left and right halves of the histogram
    # These will be the starting point for the left and right lines
    midpoint = len(histogram) // 2
    #print("midpoint=",midpoint)
    leftx_base = np.argmax(histogram[:midpoint])
    rightx_base = np.argmax(histogram[midpoint:]) + midpoint

    # Set height of windows
    window_height = np.int(height / n_windows)

    # Identify the x and y positions of all nonzero pixels in the image
    nonzero = birdeye_binary.nonzero()
    nonzero_y = np.array(nonzero[0])
    nonzero_x = np.array(nonzero[1])

    # Current positions to be updated for each window
    leftx_current = leftx_base
    rightx_current = rightx_base

    margin = 100  # width of the windows +/- margin
    minpix = 50   # minimum number of pixels found to recenter window
    
    # Create empty lists to receive left and right lane pixel indices
    left_lane_inds = []
    right_lane_inds = []
    avg_left_lane=[]
    avg_right_lane=[]
    global lane_left,lane_right
    # Step through the windows one by one
    for window in range(n_windows):
        # Identify window boundaries in x and y (and right and left)
        
        win_y_low = height - (window + 1) * window_height
        win_y_high = height - window * window_height 
        
        win_xleft_low = leftx_current - margin
        win_xleft_high = leftx_current + margin
        win_xright_low = abs(rightx_current - margin)
        win_xright_high = abs(rightx_current + margin)
        # Draw the windows on the visualization image
  
        #cv2.imshow("left",out_img[win_y_low:win_y_high,win_xleft_low:win_xleft_high])
        #print(np.average(out_img[win_y_low:win_y_high,win_xleft_low:win_xleft_high]),np.isnan(np.average(out_img[win_y_low:win_y_high,win_xleft_low:win_xleft_high])))
        if np.isnan(np.average(out_img[win_y_low:win_y_high,win_xleft_low:win_xleft_high])) != True  :
            avg_left_lane.append(np.average(out_img[win_y_low:win_y_high,win_xleft_low:win_xleft_high]))
        if np.isnan(np.average(out_img[win_y_low:win_y_high,win_xright_low:win_xright_high])) != True:
            avg_right_lane.append(np.average(out_img[win_y_low:win_y_high,win_xright_low:win_xright_high]))
        out_img=cv2.rectangle(out_img, (win_xleft_low, win_y_low), (win_xleft_high, win_y_high), (0, 255, 0), 2)
        #avg_lane_right=np.mean(perspective_tranfrom(birdeye_binary,win_xright_low,win_y_low,win_xright_high,win_y_high,300,margin))
        out_img=cv2.rectangle(out_img, (win_xright_low, win_y_low), (win_xright_high, win_y_high), (0,255, 0), 2)
        #print(avg_lane_left,avg_lane_right)
        # Identify th,e nonzero pixels in x and y within the window
        good_left_inds = ((nonzero_y >= win_y_low) & (nonzero_y < win_y_high) & (nonzero_x >= win_xleft_low)
                          & (nonzero_x < win_xleft_high)).nonzero()[0]
        good_right_inds = ((nonzero_y >= win_y_low) & (nonzero_y < win_y_high) & (nonzero_x >= win_xright_low)
                           & (nonzero_x < win_xright_high)).nonzero()[0]
        #print(window,"\t",avg_lane_left)
        # Append these indices to the lists
        left_lane_inds.append(good_left_inds)
        right_lane_inds.append(good_right_inds)

        # If you found > minpix pixels, recenter next window on their mean position
        if len(good_left_inds) > minpix:
            leftx_current = np.int(np.mean(nonzero_x[good_left_inds]))
            
        if len(good_right_inds) > minpix:
            rightx_current = np.int(np.mean(nonzero_x[good_right_inds]))
        #total_l.append(a)
        #total_r.append(b)
    
    # Concatenate the arrays of indices
    lane_right=np.mean(avg_right_lane)
    lane_left=np.mean(avg_left_lane)
    #print(lane_right)
    left_lane_inds = np.concatenate(left_lane_inds)
    right_lane_inds = np.concatenate(right_lane_inds)
    del avg_left_lane[:]
    del avg_right_lane[:]
    # Extract left and right line pixel positions
    line_lt.all_x, line_lt.all_y = nonzero_x[left_lane_inds], nonzero_y[left_lane_inds]
    line_rt.all_x, line_rt.all_y = nonzero_x[right_lane_inds], nonzero_y[right_lane_inds]

    detected = True
    if not list(line_lt.all_x) or not list(line_lt.all_y):
        left_fit_pixel = line_lt.last_fit_pixel
        left_fit_meter = line_lt.last_fit_meter
        detected = False
    else:
        left_fit_pixel = np.polyfit(line_lt.all_y, line_lt.all_x, 2)
        left_fit_meter = np.polyfit(line_lt.all_y * ym_per_pix, line_lt.all_x * xm_per_pix, 2)

    if not list(line_rt.all_x) or not list(line_rt.all_y):
        right_fit_pixel = line_rt.last_fit_pixel
        right_fit_meter = line_rt.last_fit_meter
        detected = False
    else:
        right_fit_pixel = np.polyfit(line_rt.all_y, line_rt.all_x, 2)
        right_fit_meter = np.polyfit(line_rt.all_y * ym_per_pix, line_rt.all_x * xm_per_pix, 2)

    line_lt.update_line(left_fit_pixel, left_fit_meter, detected=detected)
    line_rt.update_line(right_fit_pixel, right_fit_meter, detected=detected)

    # Generate x and y values for plotting
    ploty = np.linspace(0, height - 1, height)
    left_fitx = left_fit_pixel[0] * ploty ** 2 + left_fit_pixel[1] * ploty + left_fit_pixel[2]
    right_fitx = right_fit_pixel[0] * ploty ** 2 + right_fit_pixel[1] * ploty + right_fit_pixel[2]
    
    out_img[nonzero_y[left_lane_inds], nonzero_x[left_lane_inds]] = [255, 0, 0]
    out_img[nonzero_y[right_lane_inds], nonzero_x[right_lane_inds]] = [0, 0, 255]


    return line_lt, line_rt, out_img


def get_fits_by_previous_fits(birdeye_binary, line_lt, line_rt, verbose=False):
    """
    Get polynomial coefficients for lane-lines detected in an binary image.
    This function starts from previously detected lane-lines to speed-up the search of lane-lines in the current frame.
    :param birdeye_binary: input bird's eye view binary image
    :param line_lt: left lane-line previously detected
    :param line_rt: left lane-line previously detected
    :param verbose: if True, display intermediate output
    :return: updated lane lines and output image
    """
    
    height, width = birdeye_binary.shape

    left_fit_pixel = line_lt.last_fit_pixel
    right_fit_pixel = line_rt.last_fit_pixel

    nonzero = birdeye_binary.nonzero()
    nonzero_y = np.array(nonzero[0])

    nonzero_x = np.array(nonzero[1])
    margin = 100
    left_lane_inds = (
    (nonzero_x > (left_fit_pixel[0] * (nonzero_y ** 2) + left_fit_pixel[1] * nonzero_y + left_fit_pixel[2] - margin)) & (
    nonzero_x < (left_fit_pixel[0] * (nonzero_y ** 2) + left_fit_pixel[1] * nonzero_y + left_fit_pixel[2] + margin)))
    right_lane_inds = (
    (nonzero_x > (right_fit_pixel[0] * (nonzero_y ** 2) + right_fit_pixel[1] * nonzero_y + right_fit_pixel[2] - margin)) & (
    nonzero_x < (right_fit_pixel[0] * (nonzero_y ** 2) + right_fit_pixel[1] * nonzero_y + right_fit_pixel[2] + margin)))

    # Extract left and right line pixel positions
    line_lt.all_x, line_lt.all_y = nonzero_x[left_lane_inds], nonzero_y[left_lane_inds]
    line_rt.all_x, line_rt.all_y = nonzero_x[right_lane_inds], nonzero_y[right_lane_inds]

    detected = True
    if not list(line_lt.all_x) or not list(line_lt.all_y):
        left_fit_pixel = line_lt.last_fit_pixel
        left_fit_meter = line_lt.last_fit_meter
        detected = False
    else:
        left_fit_pixel = np.polyfit(line_lt.all_y, line_lt.all_x, 2)
        left_fit_meter = np.polyfit(line_lt.all_y * ym_per_pix, line_lt.all_x * xm_per_pix, 2)

    if not list(line_rt.all_x) or not list(line_rt.all_y):
        right_fit_pixel = line_rt.last_fit_pixel
        right_fit_meter = line_rt.last_fit_meter
        detected = False
    else:
        right_fit_pixel = np.polyfit(line_rt.all_y, line_rt.all_x, 2)
        right_fit_meter = np.polyfit(line_rt.all_y * ym_per_pix, line_rt.all_x * xm_per_pix, 2)

    line_lt.update_line(left_fit_pixel, left_fit_meter, detected=detected)
    line_rt.update_line(right_fit_pixel, right_fit_meter, detected=detected)

    # Generate x and y values for plotting
    ploty = np.linspace(0, height - 1, height)
    left_fitx = left_fit_pixel[0] * ploty ** 2 + left_fit_pixel[1] * ploty + left_fit_pixel[2]
    right_fitx = right_fit_pixel[0] * ploty ** 2 + right_fit_pixel[1] * ploty + right_fit_pixel[2]
    
    # Create an image to draw on and an image to show the selection window
    img_fit = np.dstack((birdeye_binary, birdeye_binary, birdeye_binary)) * 255
    window_img = np.zeros_like(img_fit)
    # Color in left and right line pixels
    mid_x_1=np.average(nonzero_x[left_lane_inds])
   
    img_fit[nonzero_y[left_lane_inds], nonzero_x[left_lane_inds]] = [255, 0, 0]
    img_fit[nonzero_y[right_lane_inds], nonzero_x[right_lane_inds]] = [0, 0, 255]

    # Generate a polygon to illustrate the search window area
    # And recast the x and y points into usable format for cv2.fillPoly()
    left_line_window1 = np.array([np.transpose(np.vstack([left_fitx - margin, ploty]))])
    left_line_window2 = np.array([np.flipud(np.transpose(np.vstack([left_fitx + margin, ploty])))])
    left_line_pts = np.hstack((left_line_window1, left_line_window2))
    right_line_window1 = np.array([np.transpose(np.vstack([right_fitx - margin, ploty]))])
    right_line_window2 = np.array([np.flipud(np.transpose(np.vstack([right_fitx + margin, ploty])))])
    right_line_pts = np.hstack((right_line_window1, right_line_window2))

    # Draw the lane onto the warped blank image
    cv2.fillPoly(window_img, np.int_([left_line_pts]), (0, 255, 0))
    cv2.fillPoly(window_img, np.int_([right_line_pts]), (0, 255, 0))

    result = cv2.addWeighted(img_fit, 1, window_img, 0.3, 0)



    return line_lt, line_rt, img_fit,mid_x_1


def draw_back_onto_the_road(img_undistorted, Minv, line_lt, line_rt, keep_state):
    """
    Draw both the drivable lane area and the detected lane-lines onto the original (undistorted) frame.
    :param img_undistorted: original undistorted color frame
    :param Minv: (inverse) perspective transform matrix used to re-project on original frame
    :param line_lt: left lane-line previously detected
    :param line_rt: right lane-line previously detected
    :param keep_state: if True, line state is maintained
    :return: color blend
    """
    height, width, _ = img_undistorted.shape

    left_fit = line_lt.average_fit if keep_state else line_lt.last_fit_pixel
    right_fit = line_rt.average_fit if keep_state else line_rt.last_fit_pixel
  
    # Generate x and y values for plotting
    ploty = np.linspace(0, height - 1, height)
    left_fitx = left_fit[0] * ploty ** 2 + left_fit[1] * ploty + left_fit[2]
    right_fitx = right_fit[0] * ploty ** 2 + right_fit[1] * ploty + right_fit[2]

    # draw road as green polygon on original frame
    road_warp = np.zeros_like(img_undistorted, dtype=np.uint8)
    pts_left = np.array([np.transpose(np.vstack([left_fitx, ploty]))])
    pts_right = np.array([np.flipud(np.transpose(np.vstack([right_fitx, ploty])))])
   
    pts = np.hstack((pts_left, pts_right))
    cv2.fillPoly(road_warp, np.int_([pts]), (0, 255, 0))
    road_dewarped = cv2.warpPerspective(road_warp, Minv, (width, height))  # Warp back to original image space

    blend_onto_road = cv2.addWeighted(img_undistorted, 1., road_dewarped, 0.3, 0)
    #cv2.imshow("blend_onto_road",blend_onto_road)
    # now separately draw solid lines to highlight them
    line_warp = np.zeros_like(img_undistorted)
    line_warp = line_lt.draw(line_warp, color=(255, 0, 0), average=keep_state)
    
    line_warp = line_rt.draw(line_warp, color=(0, 0, 255), average=keep_state)

    line_dewarped = cv2.warpPerspective(line_warp, Minv, (width, height))

    lines_mask = blend_onto_road.copy()
    idx = np.any([line_dewarped != 0][0], axis=2)
    lines_mask[idx] = line_dewarped[idx]
    

    blend_onto_road = cv2.addWeighted(src1=lines_mask, alpha=0.8, src2=blend_onto_road, beta=0.5, gamma=0.)

    return blend_onto_road

def clf_lane():
    global lane_left,lane_right,img,img2
    
    if lane_left < 99 and lane_right >100 :
        new_img=np.hstack((img2,img))
        return new_img,[0,1]
    elif lane_right < 99 and lane_left >100 :
        new_img=np.hstack((img,img2))
        return new_img,[1,0]
    elif lane_right >100 and lane_left >100 :
        new_img=np.hstack((img,img))
        return new_img,[1,1]
    else:
        new_img=np.hstack((img2,img2))
        return new_img,[0,0]
        

def prepare_out_blend_frame(blend_on_road, img_binary, img_birdeye, img_fit, line_lt, line_rt, offset_meter,frame,predict_frame,pix_pos,vehicle_position_words,mid_x_1):
    """
    Prepare the final pretty pretty output blend, given all intermediate pipeline images
    :param blend_on_road: color image of lane blend onto the road
    :param img_binary: thresholded binary image
    :param img_birdeye: bird's eye view of the thresholded binary image
    :param img_fit: bird's eye view with detected lane-lines highlighted
    :param line_lt: detected left lane-line
    :param line_rt: detected right lane-line
    :param offset_meter: offset from the center of the lane
    :return: pretty blend with all images and stuff stitched
    """
    h, w = blend_on_road.shape[:2]
    thumb_ratio = 0.2
    thumb_h, thumb_w = int(thumb_ratio * h), int(thumb_ratio * w)

    off_x, off_y = 20, 15

    # add a gray rectangle to highlight the upper area
    mask = blend_on_road.copy()
    mask = cv2.rectangle(mask, pt1=(0, 0), pt2=(w, thumb_h+2*off_y), color=(0, 0, 0), thickness=cv2.FILLED)
    blend_on_road = cv2.addWeighted(src1=mask, alpha=0.2, src2=blend_on_road, beta=0.9, gamma=0)

    # add thumbnail of binary image
   
    thumb_binary = cv2.resize(img_binary, dsize=(thumb_w, thumb_h))
    blend_on_road[off_y:thumb_h+off_y, off_x:off_x+thumb_w, :] = thumb_binary

    # add thumbnail of bird's eye view
     
    thumb_birdeye = cv2.resize(img_fit, dsize=(thumb_w, thumb_h))
    #thumb_birdeye = np.dstack([[thumb_birdeye],[thumb_birdeye],[thumb_birdeye]])*255
    blend_on_road[off_y:thumb_h+off_y, 3*off_x+2*thumb_w:3*(off_x+thumb_w), :] = thumb_birdeye
    """
    # add thumbnail of bird's eye view (lane-line highlighted)
    thumb_img_fit = cv2.resize(img_fit, dsize=(thumb_w, thumb_h))
    blend_on_road[off_y:thumb_h+off_y, 3*off_x+2*thumb_w:3*(off_x+thumb_w), :] = None
    """
    font = cv2.FONT_HERSHEY_SIMPLEX
    # add text (curvature and offset info) on the upper right of the blend
    cv2.line(blend_on_road,(int(w/2),h-130),(int(w/2),h-170),(0,255,0),5)
    cv2.line(blend_on_road,(int(pix_pos),h-130),(int(pix_pos),h-130),(0,255,0),5)
    
    cv2.putText(blend_on_road, ': {}'.format(mid_x_1), (700, 100), font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
    cv2.putText(blend_on_road, ': {}'.format(vehicle_position_words), (700, 80), font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
    cv2.putText(blend_on_road, ': {}'.format(predict_frame), (700, 50), font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
    cv2.putText(blend_on_road, 'Curvature radius L: {:.02f}m'.format(line_lt.curvature_meter), (300, 30), font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
    cv2.putText(blend_on_road, 'Curvature radius R: {:.02f}m'.format(line_rt.curvature_meter), (300,50), font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
    cv2.putText(blend_on_road, 'AVG Left: {:.02f}m'.format(lane_left), (300,80), font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
    cv2.putText(blend_on_road, 'AVG right: {:.02f}m'.format(lane_right), (300,100), font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
    mean_curvature_meter = np.mean([line_lt.curvature_meter, line_rt.curvature_meter])
    cv2.putText(blend_on_road, 'Curvature radius: {:.02f}m'.format(mean_curvature_meter), (960, 30), font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
    
    cv2.putText(blend_on_road, 'Offset from center: {:.02f}m'.format(offset_meter), (960, 50), font, 0.5, (0, 255, 0), 1, cv2.LINE_AA)

    return blend_on_road


def compute_offset_from_center(line_lt, line_rt, frame_width):
    """
    Compute offset from center of the inferred lane.
    The offset from the lane center can be computed under the hypothesis that the camera is fixed
    and mounted in the midpoint of the car roof. In this case, we can approximate the car's deviation
    from the lane center as the distance between the center of the image and the midpoint at the bottom
    of the image of the two lane-lines detected.
    :param line_lt: detected left lane-line
    :param line_rt: detected right lane-line
    :param frame_width: width of the undistorted frame
    :return: inferred offset
    """
    if line_lt.detected and line_rt.detected:
        line_lt_bottom = np.mean(line_lt.all_x[line_lt.all_y > 0.95 * line_lt.all_y.max()])
        line_rt_bottom = np.mean(line_rt.all_x[line_rt.all_y > 0.95 * line_rt.all_y.max()])
        lane_width = line_rt_bottom - line_lt_bottom
        midpoint = frame_width / 2
        offset_pix = abs((line_lt_bottom + lane_width / 2) - midpoint)
        offset_meter = xm_per_pix * offset_pix
    else:
        offset_meter = -1

    return offset_meter


def process_pipeline(frame, keep_state=True ):
    """
    Apply whole lane detection pipeline to an input color frame.
    :param frame: input color frame
    :param keep_state: if True, lane-line state is conserved (this permits to average results)
    :return: output blend with detected lane overlaid
    """

    global line_lt, line_rt, processed_frames
    
    # undistort the image using coefficients found in calibration
    img_undistorted = undistort(frame, mtx, dist, verbose=False)
  
    # binarize the frame s.t. lane lines are highlighted as much as possible
    img_binary = binarize(img_undistorted)

    # compute perspective transform to obtain bird's eye view
    img_birdeye, M, Minv = get_per(img_binary)
    
    img_birdeye_knn, M1, Minv1 = get_per_knn(img_undistorted)
    
   
    # fit 2-degree polynomial curve onto lane lines found
  
    if processed_frames == 0 :
        line_lt, line_rt, img_fit = get_fits_by_sliding_windows(img_birdeye, line_lt, line_rt, n_windows=9)
        
        lane,staus_lane=clf_lane()
        pix_pos=update_vehicle_position(frame,line_lt,line_rt)
        vehicle_position_words=chek_position_left(staus_lane)
        
        predict_frame=predict_knn("_",img_birdeye_knn)
        # compute offset in meter from center of the lane
        offset_meter = compute_offset_from_center(line_lt, line_rt, frame_width=frame.shape[1])
    
        # draw the surface enclosed by lane lines back onto the original frame
        blend_on_road = draw_back_onto_the_road(img_undistorted, Minv, line_lt, line_rt, keep_state)

        # stitch on the top of final output images from different steps of the pipeline
        mid_x_1=0
        prepare=prepare_out_blend_frame(blend_on_road, lane, img_birdeye, img_fit, line_lt, line_rt, offset_meter,img_birdeye_knn,predict_frame,pix_pos,vehicle_position_words,mid_x_1)
        
        processed_frames += 1
   
    else:
        #get_fits_by_previous_fits
        line_lt, line_rt, img_fit,mid_x_1 = get_fits_by_previous_fits(img_birdeye, line_lt, line_rt)
       

        lane,staus_lane=clf_lane()
  
        pix_pos=update_vehicle_position(frame,line_lt,line_rt)
        vehicle_position_words=chek_position_left(staus_lane)
        predict_frame=predict_knn("_",img_birdeye_knn)
        # compute offset in meter from center of the lane
        offset_meter = compute_offset_from_center(line_lt, line_rt, frame_width=frame.shape[1])
    
        # draw the surface enclosed by lane lines back onto the original frame
        blend_on_road = draw_back_onto_the_road(img_undistorted, Minv, line_lt, line_rt, keep_state)

        # stitch on the top of final output images from different steps of the pipeline
        
        prepare=prepare_out_blend_frame(blend_on_road, lane, img_birdeye, img_fit, line_lt, line_rt, offset_meter,img_birdeye_knn,predict_frame,pix_pos,vehicle_position_words,mid_x_1)
        
        if processed_frames == 5:
            processed_frames=0
        else:
            processed_frames += 1
        
    
    #cv2.imshow("imag_fit",img_fit)
    
    
    
   

    return prepare



def main(img_mag_thr):
    frame = process_pipeline(img_mag_thr, keep_state=False)
    return frame