
# -*- coding: utf-8 -*-
from darkflow.net.build import TFNet
import cv2
import numpy as np 
import json 
import time

from PIL import Image
options = {"model": "cfg/tiny-yolo-voc-3c.cfg", "load": "bin/tiny-yolo-voc.weights","batch": 8, \
    "epoch": 100,"gpu": 1.0,"train": True,\
    "annotation":"annotations" ,"dataset": "images"}
tfnet = TFNet(options)
tfnet.train()