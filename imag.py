import numpy as np
import cv2
import math
import os,subprocess ,time
import matplotlib.pyplot as plt
from darkflow.net.build import TFNet

import skvideo.io
from tqdm import trange
from subprocess import call 
from multiprocessing.pool import ThreadPool
import multiprocessing 
#sys.path.append("C://Users//specter//Desktop//pe_cat//darkflow//")
from line  import *

lower = np.array([17, 15, 100], dtype = "uint8")
upper = np.array([50, 56, 200], dtype = "uint8")
results=None
colors = [tuple(255 * np.random.rand(3)) for _ in range(10)]
tl1,br1=[],[]

def detect_red(frame):
    global results
   
    for result in results:
        if result['label']== 'traffic light' :
            tl = (result['topleft']['x'], result['topleft']['y'])
            br = (result['bottomright']['x'], result['bottomright']['y'])
            pts1 = np.float32([[result['topleft']['x'],result['topleft']['y']],[result['bottomright']['x'],result['topleft']['y']],[result['topleft']['x'],result['bottomright']['y']],[result['bottomright']['x'],result['bottomright']['y']]])
            pts2 = np.float32([[0,0],[300,0],[300,300],[0,300]])
            M =  cv2.getPerspectiveTransform(pts1,pts2)
            q = cv2.warpPerspective(frame, M, (300, 300))
            frame = cv2.rectangle(frame, tl, br, (0,0,255), 1)
            plt.imshow(q)
            plt.show()
            
    return frame

def Estimating_the_distance(img):
    global results,red,tl1,br1
    
    pts1 = np.float32([[375,480],[905,480],[1811,685],[-531,685]])
    pts2 = np.float32([[0,0],[500,0],[500,600],[0,600]])
    print(br1)
    for i,j in zip(tl1,br1):
        M =  cv2.getPerspectiveTransform(pts1,pts2)
        rotated_point = M.dot(np.array(((j) + (1,))))
        
        result = cv2.warpPerspective(img, M, (500, 600))
        x,y=int(rotated_point[0]/rotated_point[2]),int(rotated_point[1]/rotated_point[2])
        print("x,y:",x,y )
        
        if (0 < x <= 500) and (0 < y <= 600)  :
            print((600-y)*( 30 / 720  ))
            text="{:.0f}".format((600-y)*( 30 / 720  ))
            cv2.putText(img,str(text), (j), cv2.FONT_HERSHEY_COMPLEX, 1, 255)
    
            print("T")
        else: 
            print("F")
        
    """
    result = cv2.warpPerspective(img, M, (500, 600))
    hsv_red = cv2.cvtColor(result,cv2.COLOR_BGR2HSV)
    lower_red = np.array([0,120,70])
    upper_red = np.array([10,255,255])
    
    mask = cv2.inRange(hsv_red, lower_red, upper_red)
    th1 = cv2.threshold(mask,127,255,cv2.THRESH_BINARY)
    res = cv2.bitwise_and(result,result, mask= mask)

    redcnts =cv2.findContours(th1.copy(),
                              cv2.RETR_EXTERNAL,
                              cv2.CHAIN_APPROX_SIMPLE)[-2]
    
    if len(redcnts)>0:
        red_area = max(redcnts, key=cv2.contourArea)
        (xg,yg,wg,hg) = cv2.boundingRect(red_area)
        cv2.rectangle(result,(xg,yg),(xg+wg, yg+hg),(0,255,0),2)
        print(abs(((yg+hg)-720)*(30 / 720)))
    

    plt.subplot(121),plt.imshow(img),plt.title('Input')
    plt.subplot(122),plt.imshow(th3),plt.title('Output')
    plt.show() 
    """    
    return img

def result_predit(results,frame):
    global tl1,br1
    ym_per_pix = 30 / 720
    stime = time.time()
    num_of_center=[]
    print(results)
    for color, result in zip(colors, results):
                if result['label'] == 'car' or result['label']== 'traffic light'  :
                    if result['bottomright']['y'] >= 480:
                        tl = (result['topleft']['x'], result['topleft']['y'])
                        br = (result['bottomright']['x'], result['bottomright']['y'])
                        mid_x=int((result['topleft']['x']+result['bottomright']['x'])/2)
                        mid_y=int((result['topleft']['y']+result['bottomright']['y'])/2)
                        #root((x2-x1)^2+(y2-y1)^2)
                        br1.append(br)
                        tl1.append(tl)
                        apx=(math.sqrt((abs(mid_x - 640)**2)+(abs(mid_y - 720) **2)))/1280
                        print("apx-",apx)
                        #apx_distance = round(((1 - (result['topleft']['x']-result['bottomright']['x']))),1)
                
                        label = result['label']
                        frame=cv2.line(frame,(640,720),(mid_x,mid_y),(255,0,0),1)
                        confidence = result['confidence']
                        text = '{}: {:.0f} %'.format(label, confidence * 100)
                        #text = '{}M'.format(apx*ym_per_pix)
                        frame = cv2.rectangle(frame, tl, br, (0,0,255), 5)
                        
                         
    
                        """
                        frame = cv2.putText(
                            frame, text, tl, cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 2)
                        """
                        num_of_center.append(abs(result['bottomright']['y'] + result['bottomright']['x'])/2)

                    else:
                        pass
              
    
    return frame,num_of_center

def clear(): 
    # check and make call for specific operating system 
    os.system('cls')



def imag_clf_lane(n):
    global results
    cap=cv2.imread(n)
    #cap = cv2.VideoCapture(n)
    options = {"model": "cfg/yolo.cfg", "load": "bin/yolo.weights", "threshold": 0.1,"gpu": 1.0}                                
    tfnet = TFNet(options)
    outputfile = "11.mp4" 
    #length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    resize_h, resize_w = 720,1280
    writer = skvideo.io.FFmpegWriter(outputfile, outputdict={
    '-vcodec': 'libx264',  #use the h.264 codec
    '-crf': '25',           #set the constant rate factor to 0, which is lossless
    '-preset':'ultrafast'   #the slower the better compression, in princple, try 
                            #other options see https://trac.ffmpeg.org/wiki/Encode/H.264
    }) 
    #t = trange(length, desc='Bar desc', leave=True)
    
    num_processes = 4
    pool = ThreadPool(processes=4)
    resize_h, resize_w = 720,1280
    frame= cv2.resize(cap, (resize_w, resize_h))
    #async_result = pool.apply_async(main, (frame,))
    #img=async_result.get()
    frame= cv2.resize(frame, (resize_w, resize_h))
    results = tfnet.return_predict(frame)
    img,center=result_predit(results,frame)
    img= Estimating_the_distance(img)
    #img=detect_red(img)
    plt.imshow(img)
    plt.show()
    async_result = pool.apply_async(main, (img,))
    img=async_result.get()
    
    #writer.writeFrame(img[:,:,::-1])
    
    
    #cv2.waitKey(0)
    """
    pool = ThreadPool(processes=4)
    for _ in t:
        _, frame = cap.read()
        frame= cv2.resize(frame, (resize_w, resize_h))
        results = tfnet.return_predict(frame)
        img,center=result_predit(results,frame)
        img= Estimating_the_distance(img)
        
        async_result = pool.apply_async(main, (img,))
        img=async_result.get()
        img = np.asarray(img)
        #img=main(frame)
        
        writer.writeFrame(img[:,:,::-1])
        
        clear() 
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break   
    
    writer.close()
    cap.release()
    cv2.destroyAllWindows()
    
    """




if __name__ == "__main__":
    imag_pre()
    ret, mtx, dist, rvecs, tvecs=test_img()
    imag_clf_lane("test.jpg")
    


