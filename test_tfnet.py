from darkflow.net.build import TFNet
import cv2
import numpy as np
import matplotlib.pyplot as plt

def color(frameBGR):
    hsv = cv2.cvtColor(frameBGR, cv2.COLOR_BGR2HSV)
    
    # HSV values to define a colour range.
    colorLow = np.array([200,255,255])
    colorHigh = np.array([255,255,255])
    mask = cv2.inRange(hsv, colorLow, colorHigh)
    plt.imshow(frameBGR)
    plt.show()
    plt.imshow(mask)
    plt.show() 
    return mask

def boxing(original_img, predictions):
    newImage = np.copy(original_img)
    top_x = predictions['topleft']['x']
    top_y = predictions['topleft']['y']

    btm_x = predictions['bottomright']['x']
    btm_y = predictions['bottomright']['y']
    pts1 = np.float32([[top_x,top_y],[ btm_x,top_y],[top_x,btm_y],[btm_x,btm_y]])
    pts2 = np.float32([[0,0],[300,0],[0,300],[300,300]])
    M = cv2.getPerspectiveTransform(pts1,pts2)
    dst = cv2.warpPerspective(newImage,M,(300,300))
    confidence = predictions['confidence']
    label = predictions['label'] + " " + str(round(confidence, 3))

    if confidence > 0.3:
        newImage = cv2.rectangle(newImage, (top_x, top_y), (btm_x, btm_y), (255,0,0), 3)
        newImage = cv2.putText(newImage, label, (top_x, top_y-5), cv2.FONT_HERSHEY_COMPLEX_SMALL , 0.8, (0, 230, 0), 1, cv2.LINE_AA)
            
    return newImage,dst

options = {"model": "cfg/yolo.cfg", "load": "bin/yolo.weights", "threshold": 0.1}

tfnet = TFNet(options)

imgcv = cv2.imread("traffic.jpg")
result = tfnet.return_predict(imgcv)
fig=plt.figure(figsize=(2, 2))
for i in result:
    
    if i['label'] == 'traffic light' :
        print(i)
        
        new,dst=boxing(imgcv,i)
        dst=color(dst)
        
        
        
        
    
"""
new=boxing(imgcv,result["label"])
plt.imshow(new)
plt.show()
"""
