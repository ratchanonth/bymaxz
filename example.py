from sklearn.neighbors import KNeighborsClassifier
from classification import Classification
import cv2

#load class
my_class = Classification()

print("READY")

image = cv2.imread('examples/dashed.jpg') #Image size sure be 300*300
answer = my_class.lines_predict(image) #for predict line only !
print('real answer is dashed | classfication answer =',answer)

image = cv2.imread('examples/straight.jpg') #Image size sure be 300*300
answer = my_class.lines_predict(image) #for predict line only !
print('real answer is straight | classfication answer =',answer)

image = cv2.imread('examples/str.jpg') #Image size sure be 300*300
answer = my_class.road_predict(image) #for sign on road line only !
print('real answer is str | classfication answer =',answer)

image = cv2.imread('examples/str_and_left.jpg') #Image size sure be 300*300
answer = my_class.road_predict(image) #for sign on road line only !
print('real answer is str_and_left | classfication answer =',answer)