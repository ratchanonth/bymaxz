import numpy as np
import cv2
import matplotlib.pyplot as plt 




def thresh_frame_in_HSV(frame, min_values, max_values, verbose=False):
    """
    Threshold a color frame in HSV space
    """
    HSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    min_th_ok = np.all(HSV > min_values, axis=2)
    max_th_ok = np.all(HSV < max_values, axis=2)

    out = np.logical_and(min_th_ok, max_th_ok)

    
 
       
    return out
def undistort(frame, mtx, dist, verbose=False):
    """
    Undistort a frame given camera matrix and distortion coefficients.
    :param frame: input frame
    :param mtx: camera matrix
    :param dist: distortion coefficients
    :param verbose: if True, show frame before/after distortion correction
    :return: undistorted frame
    """
    frame_undistorted = cv2.undistort(frame, mtx, dist, newCameraMatrix=mtx)


    return frame_undistorted


def get_per(img):
        h, w = img.shape[:2]
        src = np.float32([[1000, h-150],    # br
                [280, h-150],    # bl
                [590, 410],   # tl
                [700, 410]])  # tr
        dst = np.float32([[w, h],       # br
                [0, h],       # bl
                [0, 0],       # tl
                [w, 0]])      # tr
        img_size = (img.shape[1], img.shape[0])
        M_persp = cv2.getPerspectiveTransform(src, dst)
        Minv_persp = cv2.getPerspectiveTransform(dst, src)
        
        binary_warped = cv2.warpPerspective(img, M_persp, img_size, flags=cv2.INTER_LINEAR)
        img_unpersp = cv2.warpPerspective(binary_warped, Minv_persp, img_size, flags=cv2.INTER_LINEAR)
       
        return binary_warped,M_persp,Minv_persp




def sobel_x(img, kernel_size):
    
        # 1) Convert to grayscale
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        sobel_x = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=kernel_size)
        sobel_y = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=kernel_size)
        sobel_mag = np.sqrt(sobel_x ** 2 + sobel_y ** 2)
        sobel_mag = np.uint8(sobel_mag / np.max(sobel_mag) * 255)
        
        _, sobel_mag = cv2.threshold(sobel_mag, 10, 1, cv2.THRESH_BINARY)
       
        return sobel_mag.astype(bool)

    

        

def get_binary_from_equalized_grayscale(frame):
    """
    Apply histogram equalization to an input frame, threshold it and return the (binary) result.
    """
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray,(5,5),0)
    eq_global = cv2.equalizeHist(gray)
    
    _, th = cv2.threshold(eq_global, thresh=150, maxval=255, type=cv2.THRESH_BINARY)
    
    return th

def binarize(img, verbose=False):
        """
        Convert an input frame to a binary image which highlight as most as possible the lane-lines.
        :param img: input color frame
        :param verbose: if True, show intermediate results
        :return: binarized frame
        """
      
        h, w = img.shape[:2]

        binary = np.zeros(shape=(h, w), dtype=np.uint8)
        yellow_HSV_th_min = np.array([15, 70, 70])
        yellow_HSV_th_max = np.array([40, 255, 255])
        # highlight yellow lines by threshold in HSV color space
        HSV_yellow_mask = thresh_frame_in_HSV(img, yellow_HSV_th_min, yellow_HSV_th_max, verbose=False)
        
        binary = np.logical_or(binary, HSV_yellow_mask)
        
        
        # highlight white lines by thresholding the equalized frame
        eq_white_mask = get_binary_from_equalized_grayscale(img)
        binary = np.logical_or(binary, eq_white_mask)
        
        # get Sobel binary mask (thresholded gradients)
        sobel_mask = sobel_x(img, kernel_size=9)
        
        #binary = np.logical_or(binary, sobel_mask)
        
        # apply a light morphology to "fill the gaps" in the binary image
        kernel = np.ones((9, 9), np.uint8)
        closing = cv2.morphologyEx(binary.astype(np.uint8), cv2.MORPH_CLOSE, kernel)
       
        return closing

